## 문제 1 

주식회사 모인에 근무하는 개발자 **이아무개**씨는 직장 동료들과의 친분을 다지기위해 인스타그램과 비슷한 사내 SNS를 만드려고 합니다. 

이 간단한 앱은 *최소* **users, tags, photos** 테이블을 갖고 있어야 합니다. 

User는 Photo를 여러개 업로드 할 수 있으며, 

### 최소 조건 

- users 테이블은 컬럼으로 id(integer), name(varchar)을 갖고 있어야 합니다. 
- users 테이블은 "김아무개", "심아무개", "이아무개", 세 사람에 대한 로우 데이터를 갖고 있어야 합니다.  
- photos 테이블은 컬럼으로 id(integer), title(varchar), file_name(varchar)을 갖고 있어야 합니다. 
- photos 테이블은 임의의 title 과 file_name을 가진 로우 데이터가 *5*개 이상 존재해야 합니다.  
- tags 테이블은 컬럼으로 id(integer), name(varchar)을 갖고 있어야 합니다. 
- tags 테이블은 'funny', 'happy', 'birthday' name을 가진 *3*개의 로우 데이터가 존재해야 합니다. 
- 모든 User는 최소 하나의 Photo를 업로드 했습니다. 
- 모든 Photo는 최소 하나의 tag를 갖고 있습니다.         

이아무개 씨를 위해 같은 폴더내에 있는 (db.sql 파일)에 위의 지문에 맞는 테이블을 만들고, (mysql, postgre, mariadb중 택1) 각 테이블에 최소 조건에 알맞는 데이터를 넣는 sql문을 작성해주세요. 

 
## 문제 2 

'happy'라는 name을 가진 Tag를 갖고 있는 Photo(s)의 목록을 조회하는 쿼리를 작성해주세요. (query.sql 파일 사용)

## 문제 3 

업로드를 한 사람이 “이아무개“씨인 Photo를 검색하는 쿼리를 만들어주세요. (query.sql 파일 사용)
