const { Queue, alterate } = require('../queue');

test('queue 인스턴스는 peek 메소드를 갖고 있어야 합니다.', () => {
  const queue = new Queue();
  expect(typeof queue.peek).toEqual('function');
});

test('peek 함수는 첫번 째 값을 되돌려주어야 하며, 해당 값을 삭제해서는 안됩니다.', () => {
  const queue = new Queue();
  queue.add(1);
  queue.add(2);
  expect(queue.peek()).toEqual(1);
  expect(queue.peek()).toEqual(1);
  expect(queue.remove()).toEqual(1);
  expect(queue.remove()).toEqual(2);
});

test('alterate 함수는 반드시 존재해야 합니다.', () => {
  expect(typeof alterate).toEqual('function');
});

test('alterate 함수는 두개 다른 Queue 인스턴스를 하나의 Queue로 통합해야 합니다. 이 때, 통합 된 queue는 인자로 받은 인스턴스들이 포함하고 있는 데이터를 갖고 있어야 합니다.', () => {
  const queue1 = new Queue();
  queue1.add(1);
  queue1.add(2);
  queue1.add(3);
  queue1.add(4);

  const queue2 = new Queue();
  queue2.add('one');
  queue2.add('two');
  queue2.add('three');
  queue2.add('four');

  const result = alterate(queue1, queue2);

  expect(result.remove()).toEqual(1);
  expect(result.remove()).toEqual('one');
  expect(result.remove()).toEqual(2);
  expect(result.remove()).toEqual('two');
  expect(result.remove()).toEqual(3);
  expect(result.remove()).toEqual('three');
  expect(result.remove()).toEqual(4);
  expect(result.remove()).toEqual('four');
  expect(result.remove()).toBeUndefined();
});
