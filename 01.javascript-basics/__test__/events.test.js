const Events = require('../events');

test('Events를 등록 후 callback 을 trigger 해야 합니다.', () => {
  const events = new Events();

  const cb1 = jest.fn();

  events.on('click', cb1);
  events.trigger('click');

  expect(cb1.mock.calls.length).toBe(1);
});

test('여러개의 Events들을 등록 후 callback 을 trigger 해야 합니다.', () => {
  const events = new Events();

  const cb1 = jest.fn();
  const cb2 = jest.fn();

  events.on('click', cb1);
  events.on('click', cb2);
  events.trigger('click');

  expect(cb1.mock.calls.length).toBe(1);
  expect(cb2.mock.calls.length).toBe(1);
});

test('Events can be triggered multiple times', () => {
  const events = new Events();

  const cb1 = jest.fn();
  const cb2 = jest.fn();

  events.on('click', cb1);
  events.trigger('click');
  events.on('click', cb2);
  events.trigger('click');
  events.trigger('click');

  expect(cb1.mock.calls.length).toBe(3);
  expect(cb2.mock.calls.length).toBe(2);
});

test('이벤트는 다른 이름들을 갖고 있을 수 있습니다.', () => {
  const events = new Events();

  const cb1 = jest.fn();
  const cb2 = jest.fn();

  events.on('click', cb1);
  events.trigger('click');
  events.on('hover', cb2);
  events.trigger('click');
  events.trigger('hover');

  expect(cb1.mock.calls.length).toBe(2);
  expect(cb2.mock.calls.length).toBe(1);
});

test('이벤트는 삭제될 수 있어야합니다.', () => {
  const events = new Events();

  const cb1 = jest.fn();
  const cb2 = jest.fn();

  events.on('hover', cb2);

  events.on('click', cb1);
  events.trigger('click');
  events.off('click');
  events.trigger('click');

  events.trigger('hover');

  expect(cb1.mock.calls.length).toBe(1);
  expect(cb2.mock.calls.length).toBe(1);
});
