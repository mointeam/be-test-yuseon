## 과제설명

주식회사 모인의 개발자인 김아무개씨는 같은 회사 마케팅 팀으로부터 MOIN 굿즈 스토어의 판매 실적을 분석하고, 주간 판매 데이터를 등록할 수 있는 서비스 개발을 의뢰받았습니다. 불운하게도 김아무개씨는 환절기에 감기 몸살이 걸려 당분간 일을 할 수 없게 되었습니다. 동료 개발자인 김아무개씨 대신 이 서비스를 기한에 늦지 않게 개발해주세요!

<br>

## 데이터셋

아래 두 개의 데이터셋을 다운받아 받아주세요. 이 데이터셋들의 출처는 Kaggle입니다. 혹시 링크가 망가졌을 시 이메일로 문의 부탁드립니다. <br>

1. https://drive.google.com/file/d/1WSGFl-13dUcRKjsKRxjX0rAwhcdpNoXB/view?usp=sharing <br>

2. https://drive.google.com/file/d/1V_gH76gvYl7X_gCllG94XSyC_1VcmFXR/view?usp=sharing <br>

<br>

## 요구사항

1. 제공된 데이터에 맞게 DB 스키마를 설계해주시고, 데이터를 시딩하는 코드를 작성해주세요.

2. 아래 'API 개발' 파트를 읽어보시고 Rest API를 작성해주세요.

3. Docker를 이용해 앱을 컨테이너화 해주세요.

<br>

## 참고사항

1. 원하시는 언어/프레임워크를 사용하여 개발해주세요. DB나 ORM도 원하시는 대로 사용하셔도 좋습니다. 

2. 제공된 데이터를 저장 할 테이블 스키마 설계를 표현 하는 문서를 작성해주세요.

3. 작성하신 API에 대한 간략한 문서화 및 테스트 코드를 작성해주세요.

4. RESTful API 형태로 response body를 serialize해서 리턴해주시면 좋습니다.

5. 간단한 기능이지만 유지보수를 고려한 설계를 해주시면 더욱 좋습니다.

<br>

## API 개발

1. (GET) /analysis : 특정 기간을 입력받아 해당 기간 내 Store 별로 총 weekly sale 과 average cpi 를 불러올 수 있는 기능을 구현 해 주세요.

   - request query
     from: 검색 시작 날짜 (해당 날짜 포함)
     to: 검색 끝 날짜 (해당 날짜 미포함)

   - response body (json format)
     {store id}: store id
     sales_total: 해당 store에 속한 dept들의 의 검색 기간 내 weekly_sales 의 총 합
     average_CPI: 해당 store에 속한 dept들의 검색 기간 내 CPI(Consumer Price Index) 의 평균

<br>

   ### Request/Response 예시
   ### Request URI
```json
{
  http://localhost:8080/analysis/?from=12/03/2010&to=12/03/2011
}
```

### Response

```json
  {
    1: {
    "sales_total": 856423,
    "average_CPI": 221.3112
    },
    2: {
    "sales_total": 1056429,
    "average_CPI": 191.5
    },
    3: {
    "sales_total": 3564427,
    "average_CPI": 238.1312
    },
    … (생략)
  }
```

<br>

2. (POST) /weekly-sales : 주간 sales 데이터를 추가하는 기능 개발

### Request/Response 예시
### Request

```json
{
  "store_id": 1,
  "dept": 1,
  "date": "2021-03-30T17:05:27.060Z",
  "weekly_sales": 504721
}
```

### Response

```json
{
  "store_id": 1,
  "dept": 1,
  "date": "2021-03-30T17:05:27.060Z",
  "weekly_sales": 504721
}
```
