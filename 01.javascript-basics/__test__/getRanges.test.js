const getRanges = require('../getRanges');

test('nums에 value와 일치하는 값이 있을 경우 시작과 끝 지점을 배열형태로 돌려주어야 합니다.', () => {
  expect(typeof getRanges).toEqual('function');
});

test('nums에 value와 일치하는 값이 있을 경우 시작과 끝 지점을 배열형태로 돌려주어야 합니다.', () => {
  expect(getRanges([1, 2, 4, 4, 4, 5, 6, 7], 4)).toEqual([2, 4]);
});

test('nums에 value와 일치하는 값이 있을 경우 시작과 끝 지점을 배열형태로 돌려주어야 합니다.', () => {
  expect(getRanges([5, 6, 11, 11], 11)).toEqual([2, 3]);
});

test('nums에 value와 일치하는 값이 있을 경우 시작과 끝 지점을 배열형태로 돌려주어야 합니다.', () => {
  expect(getRanges([1, 2, 3, 4, 5, 5, 6, 7, 8, 9], 9)).toEqual([9, 9]);
});

test('nums에 객체중 주어진 value 값과 일치하는 숫자가 없는 경우 [-1, -1]을 돌려주어야 합니다.', () => {
  expect(getRanges([1, 2, 2, 8, 8, 10], 6)).toEqual([-1, -1]);
});

test('nums에 빈 객체를 받을 경우 [-1, -1]을 돌려주어야 합니다.', () => {
  expect(getRanges([], 1)).toEqual([-1, -1]);
});
