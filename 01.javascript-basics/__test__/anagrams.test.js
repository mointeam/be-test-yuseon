const anagrams = require('../anagrams');

test('anagrams 함수가 존재해야 합니다.', () => {
  expect(typeof anagrams).toEqual('function');
});

test('"hello" 는 "llohe" 의 anagram 입니다.', () => {
  expect(anagrams('hello', 'llohe')).toBeTruthy();
});

test('"Whoa! Hi!" 는 "Hi! Whoa!" 의 anagram 입니다.', () => {
  expect(anagrams('Whoa! Hi!', 'Hi! Whoa!')).toBeTruthy();
});

test('"One One" 와 "Two two two" 는 서로 일치하지 않습니다.', () => {
  expect(anagrams('One One', 'Two two two')).toBeFalsy();
});

test('"One one" 와 "One one c" 는 서로 일치하지 않습니다.', () => {
  expect(anagrams('One one', 'One one c')).toBeFalsy();
});

test('"A tree, a life, a bench" 와 "A tree, a fence, a yard" 는 서로 일치하지 않습니다.', () => {
  expect(
    anagrams('A tree, a life, a bench', 'A tree, a fence, a yard'),
  ).toBeFalsy();
});
