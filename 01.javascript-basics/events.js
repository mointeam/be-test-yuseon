class Events {
  //TODO event를 등록하는 메소드를 작성해주세요.
  on(event_name, callback) {}

  //TODO event_name 이 들어왔을 때 해당 event에 관련 된 모든 callback 함수를 호출하는 메소드를 작성해주세요.
  trigger(event_name) {}

  // TODO 주어진 event_name과 연관 된 모든 handler(callback 함수)들을 제거하는 메소드를 작성해주세요.
  off(event_name) {}
}

// module.exports = Events;
