class Queue {
  constructor() {
    this.data = [];
  }

  add(task) {
    this.data.unshift(task);
  }

  remove() {
    return this.data.pop();
  }

  peek() {
    // 가장 먼저 추가 된 task 를 돌려주는 peek 메소드를 구현해주세요.
  }
}

function alterate(task1, task2) {
  // 두개의 다른 queue 인스턴스를 합성하는 alterate 함수를 구현해주세요.
}

module.exports = { Queue, alterate };
